FROM node:12-buster

RUN apt-get update
RUN apt-get install -y python-dev python-pip zip jq
RUN cd /tmp
RUN pip install awscli --upgrade
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
